//
//  AppDelegate.h
//  SJLiveVideo
//
//  Created by king on 16/6/14.
//  Copyright © 2016年 king. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

